package com.iguanafix.reseller.api.model.request;

/**
 * Data related to client, used in an Order creation request.
 *
 * @see Order
 */
public class Client {

    private String address;
    private String altPhone;
    private String department;
    private String docNumber;
    private String docType;
    private String email;
    private String firstName;
    private String floor;
    private String lastName;
    private Double lat;
    private Double lng;
    private String phone;
    private String zipCode;

    /**
     * Gets the value of the address property.
     *
     * @return Complete address for where the service will take place.
     *
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     *
     * @param value Complete address for where the service will take place.
     *
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the altPhone property.
     *
     * @return Alternative customer phone number.
     *
     */
    public String getAltPhone() {
        return altPhone;
    }

    /**
     * Sets the value of the altPhone property.
     *
     * @param value Alternative customer phone number.
     *
     */
    public void setAltPhone(String value) {
        this.altPhone = value;
    }

    /**
     * Gets the value of the department property.
     *
     * @return Department identifier for where the service will take place, if necessary.
     *
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Sets the value of the department property.
     *
     * @param value Department identifier for where the service will take place, if necessary.
     *
     */
    public void setDepartment(String value) {
        this.department = value;
    }

    /**
     * Gets the value of the docNumber property.
     *
     * @return Document number of the client receiving the service. Should be correctly formatted.
     *
     */
    public String getDocNumber() {
        return docNumber;
    }

    /**
     * Sets the value of the docNumber property.
     *
     * @param value Document number of the client receiving the service. Should be correctly formatted.
     *
     */
    public void setDocNumber(String value) {
        this.docNumber = value;
    }

    /**
     * Gets the value of the docType property.
     *
     * @return Document type of the client receiving the service.
     *
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Sets the value of the docType property.
     *
     * @param value Document type of the client receiving the service.
     *
     */
    public void setDocType(String value) {
        this.docType = value;
    }

    /**
     * Gets the value of the email property.
     *
     * @return Client email address. Must be a valid email.
     *
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     *
     * @param value Client email address. Must be a valid email.
     *
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the firstName property.
     *
     * @return Client first name.
     *
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     *
     * @param value Client first name.
     *
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the floor property.
     *
     * @return Floor identifier for where the service will take place, if necessary.
     *
     */
    public String getFloor() {
        return floor;
    }

    /**
     * Sets the value of the floor property.
     *
     * @param value Floor identifier for where the service will take place, if necessary.
     *
     */
    public void setFloor(String value) {
        this.floor = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return Client last name.
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value Client last name.
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the lat property.
     * 
     * @return Latitude where service will take place, if available. Required if lng is sent.
     *     
     */
    public Double getLat() {
        return lat;
    }

    /**
     * Sets the value of the lat property.
     * 
     * @param value Latitude where service will take place, if available. Required if lng is sent.
     *     
     */
    public void setLat(Double value) {
        this.lat = value;
    }

    /**
     * Gets the value of the lng property.
     * 
     * @return Longitude where service will take place, if available. Required if lat is sent.
     *     
     */
    public Double getLng() {
        return lng;
    }

    /**
     * Sets the value of the lng property.
     * 
     * @param value Longitude where service will take place, if available. Required if lat is sent.
     *     
     */
    public void setLng(Double value) {
        this.lng = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return Client phone number. Must be a valid phone number.
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value Client phone number. Must be a valid phone number.
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the zipCode property.
     * 
     * @return Client phone number. Must be a valid phone number.
     *     
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * Sets the value of the zipCode property.
     * 
     * @param value Client phone number. Must be a valid phone number.
     *     
     */
    public void setZipCode(String value) {
        this.zipCode = value;
    }

    @Override
    public String toString() {
        return "{" +
                "\"address\":" + (address == null ? "null" : "\"" + address + "\"") + "," +
                "\"altPhone\":" + (altPhone == null ? "null" : "\"" + altPhone + "\"") + "," +
                "\"department\":" + (department == null ? "null" : "\"" + department + "\"") + "," +
                "\"docNumber\":" + (docNumber == null ? "null" : "\"" + docNumber + "\"") + "," +
                "\"docType\":" + (docType == null ? "null" : "\"" + docType + "\"") + "," +
                "\"email\":" + (email == null ? "null" : "\"" + email + "\"") + "," +
                "\"firstName\":" + (firstName == null ? "null" : "\"" + firstName + "\"") + "," +
                "\"floor\":" + (floor == null ? "null" : "\"" + floor + "\"") + "," +
                "\"lastName\":" + (lastName == null ? "null" : "\"" + lastName + "\"") + "," +
                "\"lat\":" + (lat == null ? "null" : "\"" + lat + "\"") + "," +
                "\"lng\":" + (lng == null ? "null" : "\"" + lng + "\"") + "," +
                "\"phone\":" + (phone == null ? "null" : "\"" + phone + "\"") + "," +
                "\"zipCode\":" + (zipCode == null ? "null" : "\"" + zipCode + "\"") +
                "}";
    }
}
