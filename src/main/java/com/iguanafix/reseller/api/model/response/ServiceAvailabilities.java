package com.iguanafix.reseller.api.model.response;

import com.iguanafix.reseller.api.model.response.base.BaseResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Response for service availabilities endpoint (v1/product/{productInstallationId}/find-availability).
 */
public class ServiceAvailabilities extends BaseResponse {

    private List<ServiceAvailability> availability;

    /**
     * Gets the value of the availability property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the availability property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAvailability().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceAvailability }
     * 
     * @return Available service schedule information.
     */
    public List<ServiceAvailability> getAvailability() {
        if (availability == null) {
            availability = new ArrayList<ServiceAvailability>();
        }
        return this.availability;
    }

    @Override
    public String toString() {
        return "{" +
                "\"availability\":" + (availability == null ? "null" : Arrays.toString(availability.toArray())) +
                "}";
    }
}
