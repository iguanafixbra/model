package com.iguanafix.reseller.api.model.opts;

/**
 * Possible Invoice Types enumeration.
 */
public enum InvoiceType {

    NOTA_FISCAL("NOTA FISCAL");

    private String value;

    InvoiceType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
