package com.iguanafix.reseller.api.model.opts;

/**
 * Possible Document Types enumeration.
 */
public enum DocType {

    CPF("CPF"),
    CNPJ("CNPJ");

    private String value;

    DocType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
