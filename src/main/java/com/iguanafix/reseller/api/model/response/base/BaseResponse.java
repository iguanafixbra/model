package com.iguanafix.reseller.api.model.response.base;

/**
 * Base class with fields common to all API responses.
 */
public abstract class BaseResponse {

    private String responseCode;
    private String responseMessage;
    private Boolean inError;

    /**
     * Gets the value of the responseCode property.
     *
     * @return Http response code.
     *
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Sets the value of the responseCode property.
     *
     * @param value Http response code.
     *
     */
    public void setResponseCode(String value) {
        this.responseCode = value;
    }

    /**
     * Gets the value of the responseMessage property.
     *
     * @return Message explaining api result.
     *
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * Sets the value of the responseMessage property.
     *
     * @param value Message explaining api result.
     *
     */
    public void setResponseMessage(String value) {
        this.responseMessage = value;
    }

    /**
     * Gets the value of the inError property.
     *
     * @return Errors have occured during execution of this call.
     *
     */
    public Boolean getInError() {
        return inError;
    }

    /**
     * Sets the value of the inError property.
     *
     * @param inError Errors have occured during execution of this call.
     *
     */
    public void setInError(Boolean inError) {
        this.inError = inError;
    }
}
