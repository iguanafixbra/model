package com.iguanafix.reseller.api.model.opts;

/**
 * Possible Time Ranges enumeration.
 */
public enum TimeRangeId {

    MORNING("MORNING"),
    NOON("NOON"),
    AFTERNOON("AFTERNOON");

    private String value;

    TimeRangeId(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
