package com.iguanafix.reseller.api.model.response;

import com.iguanafix.reseller.api.model.response.base.BaseResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Response for available installation categories endpoint (v1/product/installation-categories/{countryCode}/{segment}).
 */
public class InstallationCategories extends BaseResponse {

    private List<InstallationCategory> instCategory;

    /**
     * Gets the value of the instCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the instCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInstCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InstallationCategory }
     *
     * @return Available Installation categories information.
     * 
     */
    public List<InstallationCategory> getInstCategory() {
        if (instCategory == null) {
            instCategory = new ArrayList<InstallationCategory>();
        }
        return this.instCategory;
    }

    @Override
    public String toString() {
        return "{" +
                "\"instCategory\":" + (instCategory == null ? "null" : Arrays.toString(instCategory.toArray())) +
                "}";
    }
}
