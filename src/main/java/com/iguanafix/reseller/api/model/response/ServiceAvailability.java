package com.iguanafix.reseller.api.model.response;

/**
 * Product installation schedule availability.
 *
 * @see ServiceAvailabilities
 */
public class ServiceAvailability {

    private String date;
    private String timeRangeId;
    private String timeRangeText;

    /**
     * Gets the value of the date property.
     * 
     * @return Available date in ISO-8601 Date Format (YYYY-MM-DD).
     *     
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets the value of the date property.
     * 
     * @param value Available date in ISO-8601 Date Format (YYYY-MM-DD).
     *     
     */
    public void setDate(String value) {
        this.date = value;
    }

    /**
     * Gets the value of the timeRangeId property.
     * 
     * @return Time range identifier.
     *     
     */
    public String getTimeRangeId() {
        return timeRangeId;
    }

    /**
     * Sets the value of the timeRangeId property.
     * 
     * @param value Time range identifier.
     *     
     */
    public void setTimeRangeId(String value) {
        this.timeRangeId = value;
    }

    /**
     * Gets the value of the timeRangeText property.
     * 
     * @return Time range description.
     *     
     */
    public String getTimeRangeText() {
        return timeRangeText;
    }

    /**
     * Sets the value of the timeRangeText property.
     * 
     * @param value Time range description.
     *     
     */
    public void setTimeRangeText(String value) {
        this.timeRangeText = value;
    }

    @Override
    public String toString() {
        return "{" +
                "\"date\":" + (date == null ? "null" : "\"" + date + "\"") + "," +
                "\"timeRangeId\":" + (timeRangeId == null ? "null" : "\"" + timeRangeId + "\"") + "," +
                "\"timeRangeText\":" + (timeRangeText == null ? "null" : "\"" + timeRangeText + "\"") +
                "}";
    }
}
