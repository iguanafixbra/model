package com.iguanafix.reseller.api.model.response;

import com.iguanafix.reseller.api.model.response.base.BaseResponse;

/**
 * Response for order related endpoints.
 */
public class OrderResponse extends BaseResponse {

    private Long orderId;

    /**
     * Gets the value of the orderId property.
     *
     * @return Affected order id.
     *
     */
    public Long getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     *
     * @param value Affected order id.
     */
    public void setOrderId(Long value) {
        this.orderId = value;
    }

    @Override
    public String toString() {
        return "{" +
                "\"orderId\":" + (orderId == null ? "null" : "\"" + orderId + "\"") +
                "}";
    }
}
