package com.iguanafix.reseller.api.model.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Data related to the service execution, used in an Order creation request.
 *
 * @see Order
 */
public class ServiceData {

    private String additionalComments;
    private String countryCode;
    private String externalCityId;
    private String externalNeighbourhoodId;
    private String externalReferenceId;
    private String externalStateId;
    private String extraParam0;
    private String extraParam1;
    private String extraParam2;
    private String extraParam3;
    private String extraParam4;
    private String extraParam5;
    private String extraParam6;
    private String extraParam7;
    private String extraParam8;
    private String extraParam9;
    private String serviceDateYyyyMMdd;
    private List<ServiceQty> serviceIds;
    private String serviceTimeSlot;

    /**
     * Gets the value of the additionalComments property.
     * 
     * @return Aditional comments pertaining to the service execution.
     *     
     */
    public String getAdditionalComments() {
        return additionalComments;
    }

    /**
     * Sets the value of the additionalComments property.
     * 
     * @param value Aditional comments pertaining to the service execution.
     *     
     */
    public void setAdditionalComments(String value) {
        this.additionalComments = value;
    }

    /**
     * Gets the value of the countryCode property.
     * 
     * @return Country code for where service will be executed.
     *     
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * Sets the value of the countryCode property.
     * 
     * @param value Country code for where service will be executed.
     *     
     */
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    /**
     * Gets the value of the externalCityId property.
     * 
     * @return City id for where service will be executed.
     *     
     */
    public String getExternalCityId() {
        return externalCityId;
    }

    /**
     * Sets the value of the externalCityId property.
     * 
     * @param value City id for where service will be executed.
     *     
     */
    public void setExternalCityId(String value) {
        this.externalCityId = value;
    }

    /**
     * Gets the value of the externalNeighbourhoodId property.
     * 
     * @return Neighbourhood id for where service will be executed.
     *     
     */
    public String getExternalNeighbourhoodId() {
        return externalNeighbourhoodId;
    }

    /**
     * Sets the value of the externalNeighbourhoodId property.
     * 
     * @param value Neighbourhood id for where service will be executed.
     *     
     */
    public void setExternalNeighbourhoodId(String value) {
        this.externalNeighbourhoodId = value;
    }

    /**
     * Gets the value of the externalReferenceId property.
     * 
     * @return Reference Id used by reseller to reference this order.
     *     
     */
    public String getExternalReferenceId() {
        return externalReferenceId;
    }

    /**
     * Sets the value of the externalReferenceId property.
     * 
     * @param value Reference Id used by reseller to reference this order.
     *     
     */
    public void setExternalReferenceId(String value) {
        this.externalReferenceId = value;
    }

    /**
     * Gets the value of the externalStateId property.
     * 
     * @return State id for where service will be executed.
     *     
     */
    public String getExternalStateId() {
        return externalStateId;
    }

    /**
     * Sets the value of the externalStateId property.
     * 
     * @param value State id for where service will be executed.
     *     
     */
    public void setExternalStateId(String value) {
        this.externalStateId = value;
    }

    /**
     * Gets the value of the extraParam0 property.
     * 
     * @return Extra reseller specific parameter.
     *     
     */
    public String getExtraParam0() {
        return extraParam0;
    }

    /**
     * Sets the value of the extraParam0 property.
     * 
     * @param value Extra reseller specific parameter.
     *     
     */
    public void setExtraParam0(String value) {
        this.extraParam0 = value;
    }

    /**
     * Gets the value of the extraParam1 property.
     * 
     * @return Extra reseller specific parameter.
     *     
     */
    public String getExtraParam1() {
        return extraParam1;
    }

    /**
     * Sets the value of the extraParam1 property.
     * 
     * @param value Extra reseller specific parameter.
     *     
     */
    public void setExtraParam1(String value) {
        this.extraParam1 = value;
    }

    /**
     * Gets the value of the extraParam2 property.
     * 
     * @return Extra reseller specific parameter.
     *     
     */
    public String getExtraParam2() {
        return extraParam2;
    }

    /**
     * Sets the value of the extraParam2 property.
     * 
     * @param value Extra reseller specific parameter.
     *     
     */
    public void setExtraParam2(String value) {
        this.extraParam2 = value;
    }

    /**
     * Gets the value of the extraParam3 property.
     * 
     * @return Extra reseller specific parameter.
     *     
     */
    public String getExtraParam3() {
        return extraParam3;
    }

    /**
     * Sets the value of the extraParam3 property.
     * 
     * @param value Extra reseller specific parameter.
     *     
     */
    public void setExtraParam3(String value) {
        this.extraParam3 = value;
    }

    /**
     * Gets the value of the extraParam4 property.
     * 
     * @return Extra reseller specific parameter.
     *     
     */
    public String getExtraParam4() {
        return extraParam4;
    }

    /**
     * Sets the value of the extraParam4 property.
     * 
     * @param value Extra reseller specific parameter.
     *     
     */
    public void setExtraParam4(String value) {
        this.extraParam4 = value;
    }

    /**
     * Gets the value of the extraParam5 property.
     * 
     * @return Extra reseller specific parameter.
     *     
     */
    public String getExtraParam5() {
        return extraParam5;
    }

    /**
     * Sets the value of the extraParam5 property.
     * 
     * @param value Extra reseller specific parameter.
     *     
     */
    public void setExtraParam5(String value) {
        this.extraParam5 = value;
    }

    /**
     * Gets the value of the extraParam6 property.
     * 
     * @return Extra reseller specific parameter.
     *     
     */
    public String getExtraParam6() {
        return extraParam6;
    }

    /**
     * Sets the value of the extraParam6 property.
     * 
     * @param value Extra reseller specific parameter.
     *     
     */
    public void setExtraParam6(String value) {
        this.extraParam6 = value;
    }

    /**
     * Gets the value of the extraParam7 property.
     * 
     * @return Extra reseller specific parameter.
     *     
     */
    public String getExtraParam7() {
        return extraParam7;
    }

    /**
     * Sets the value of the extraParam7 property.
     * 
     * @param value Extra reseller specific parameter.
     *     
     */
    public void setExtraParam7(String value) {
        this.extraParam7 = value;
    }

    /**
     * Gets the value of the extraParam8 property.
     * 
     * @return Extra reseller specific parameter.
     *     
     */
    public String getExtraParam8() {
        return extraParam8;
    }

    /**
     * Sets the value of the extraParam8 property.
     * 
     * @param value Extra reseller specific parameter.
     *     
     */
    public void setExtraParam8(String value) {
        this.extraParam8 = value;
    }

    /**
     * Gets the value of the extraParam9 property.
     * 
     * @return Extra reseller specific parameter.
     *     
     */
    public String getExtraParam9() {
        return extraParam9;
    }

    /**
     * Sets the value of the extraParam9 property.
     * 
     * @param value Extra reseller specific parameter.
     *     
     */
    public void setExtraParam9(String value) {
        this.extraParam9 = value;
    }

    /**
     * Gets the value of the serviceDateYyyyMMdd property.
     * 
     * @return Date for service execution in ISO-8601 Date Format (YYYY-MM-DD).
     *     
     */
    public String getServiceDateYyyyMMdd() {
        return serviceDateYyyyMMdd;
    }

    /**
     * Sets the value of the serviceDateYyyyMMdd property.
     * 
     * @param value Date for service execution in ISO-8601 Date Format (YYYY-MM-DD).
     *     
     */
    public void setServiceDateYyyyMMdd(String value) {
        this.serviceDateYyyyMMdd = value;
    }

    /**
     * Gets the value of the serviceIds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serviceIds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServiceIds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceQty }
     * 
     * @return Service quantities hired in an order.
     *
     */
    public List<ServiceQty> getServiceIds() {
        if (serviceIds == null) {
            serviceIds = new ArrayList<ServiceQty>();
        }
        return this.serviceIds;
    }

    /**
     * Gets the value of the serviceTimeSlot property.
     * 
     * @return Time range for service execution.
     *     
     */
    public String getServiceTimeSlot() {
        return serviceTimeSlot;
    }

    /**
     * Sets the value of the serviceTimeSlot property.
     * 
     * @param value Time range for service execution.
     *     
     */
    public void setServiceTimeSlot(String value) {
        this.serviceTimeSlot = value;
    }

    @Override
    public String toString() {
        return "{" +
                "\"additionalComments\":" + (additionalComments == null ? "null" : "\"" + additionalComments + "\"") + "," +
                "\"countryCode\":" + (countryCode == null ? "null" : "\"" + countryCode + "\"") + "," +
                "\"externalCityId\":" + (externalCityId == null ? "null" : "\"" + externalCityId + "\"") + "," +
                "\"externalNeighbourhoodId\":" + (externalNeighbourhoodId == null ? "null" : "\"" + externalNeighbourhoodId + "\"") + "," +
                "\"externalReferenceId\":" + (externalReferenceId == null ? "null" : "\"" + externalReferenceId + "\"") + "," +
                "\"externalStateId\":" + (externalStateId == null ? "null" : "\"" + externalStateId + "\"") + "," +
                "\"extraParam0\":" + (extraParam0 == null ? "null" : "\"" + extraParam0 + "\"") + "," +
                "\"extraParam1\":" + (extraParam1 == null ? "null" : "\"" + extraParam1 + "\"") + "," +
                "\"extraParam2\":" + (extraParam2 == null ? "null" : "\"" + extraParam2 + "\"") + "," +
                "\"extraParam3\":" + (extraParam3 == null ? "null" : "\"" + extraParam3 + "\"") + "," +
                "\"extraParam4\":" + (extraParam4 == null ? "null" : "\"" + extraParam4 + "\"") + "," +
                "\"extraParam5\":" + (extraParam5 == null ? "null" : "\"" + extraParam5 + "\"") + "," +
                "\"extraParam6\":" + (extraParam6 == null ? "null" : "\"" + extraParam6 + "\"") + "," +
                "\"extraParam7\":" + (extraParam7 == null ? "null" : "\"" + extraParam7 + "\"") + "," +
                "\"extraParam8\":" + (extraParam8 == null ? "null" : "\"" + extraParam8 + "\"") + "," +
                "\"extraParam9\":" + (extraParam9 == null ? "null" : "\"" + extraParam9 + "\"") + "," +
                "\"serviceDateYyyyMMdd\":" + (serviceDateYyyyMMdd == null ? "null" : "\"" + serviceDateYyyyMMdd + "\"") + "," +
                "\"serviceIds\":" + (serviceIds == null ? "null" : Arrays.toString(serviceIds.toArray())) + "," +
                "\"serviceTimeSlot\":" + (serviceTimeSlot == null ? "null" : "\"" + serviceTimeSlot + "\"") +
                "}";
    }
}
