package com.iguanafix.reseller.api.model.response;

import com.iguanafix.reseller.api.model.response.base.BaseResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Response for available product installations endpoint (v1/product/installation-category/{installationCategoryId}/installation-products).
 */
public class ProductInstallations extends BaseResponse {

    private List<ProductInstallation> productInstallation;

    /**
     * Gets the value of the productInstallation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productInstallation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductInstallation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductInstallation }
     *
     * @return Available product installations information.
     */
    public List<ProductInstallation> getProductInstallation() {
        if (productInstallation == null) {
            productInstallation = new ArrayList<ProductInstallation>();
        }
        return this.productInstallation;
    }

    @Override
    public String toString() {
        return "{" +
                "\"productInstallation\":" + (productInstallation == null ? "null" : Arrays.toString(productInstallation.toArray())) +
                "}";
    }
}
