package com.iguanafix.reseller.api.model.response;

import com.iguanafix.reseller.api.model.response.base.BaseResponse;

/**
 * Response for order status endpoint (v1/order/orderId/status).
 */
public class OrderStatusResponse extends BaseResponse {

    private Long orderId;
    private String status;
    private Integer statusId;

    /**
     * Gets the value of the orderId property.
     *
     * @return Id of the requested order.
     *
     */
    public Long getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     *
     * @param value Id of the requested order.
     *
     */
    public void setOrderId(Long value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return Status for the requested order.
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value Status for the requested order.
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the statusId property.
     *
     * @return Status code for the requested order.
     * 
     */
    public Integer getStatusId() {
        return statusId;
    }

    /**
     * Sets the value of the statusId property.
     *
     * @param value Status code for the requested order.
     */
    public void setStatusId(Integer value) {
        this.statusId = value;
    }

    @Override
    public String toString() {
        return "{" +
                "\"orderId\":" + (orderId == null ? "null" : "\"" + orderId + "\"") + "," +
                "\"status\":" + (status == null ? "null" : "\"" + status + "\"") + "," +
                "\"statusId\":" + (statusId == null ? "null" : "\"" + statusId + "\"") +
                "}";
    }
}
