package com.iguanafix.reseller.api.model.request;

/**
 * Payload used when calling the mark order paid with external invoice endpoint (v1/order/orderId/mark-paid-with-invoice).
 */
public class ExternalInvoicePayment {

    private String externalInvoice;

    /**
     * Gets the value of the externalInvoice property.
     * 
     * @return External Invoice Id.
     *     
     */
    public String getExternalInvoice() {
        return externalInvoice;
    }

    /**
     * Sets the value of the externalInvoice property.
     * 
     * @param value External Invoice Id.
     *     
     */
    public void setExternalInvoice(String value) {
        this.externalInvoice = value;
    }

    @Override
    public String toString() {
        return "{" +
                "\"externalInvoice\":" + (externalInvoice == null ? "null" : "\"" + externalInvoice + "\"") +
                "}";
    }
}
