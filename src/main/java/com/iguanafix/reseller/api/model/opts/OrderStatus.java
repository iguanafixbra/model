package com.iguanafix.reseller.api.model.opts;

/**
 * Possible Order Statuses enumeration.
 */
public enum OrderStatus {

    NOT_FOUND(0, "NOT_FOUND"),
    CONFIRMED(10, "CONFIRMED"),
    PENDING(20, "PENDING"),
    PAID(30, "PAID"),
    COMPLETE(100, "COMPLETE"),
    CANCELLED(101, "CANCELLED");

    private Integer id;
    private String text;

    OrderStatus(Integer id, String text) {
        this.id = id;
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public String getText() {
        return text;
    }
}
