package com.iguanafix.reseller.api.model.request;

/**
 * Data related to invoice generation, used in an Order creation request.
 *
 * @see Order
 */
public class BillingData {

    private String billingName;
    private String docNumber;
    private String docType;
    private String invoiceType;

    /**
     * Gets the value of the billingName property.
     *
     * @return Complete name to be used when generating invoice.
     */
    public String getBillingName() {
        return billingName;
    }

    /**
     * Sets the value of the billingName property.
     *
     * @param value Complete name to be used when generating invoice.
     */
    public void setBillingName(String value) {
        this.billingName = value;
    }

    /**
     * Gets the value of the docNumber property.
     *
     * @return Document number to ve used when generating invoid. Should be correctly formatted.
     */
    public String getDocNumber() {
        return docNumber;
    }

    /**
     * Sets the value of the docNumber property.
     *
     * @param value Document number to ve used when generating invoid. Should be correctly formatted.
     */
    public void setDocNumber(String value) {
        this.docNumber = value;
    }

    /**
     * Gets the value of the docType property.
     *
     * @return Document type to be used when generating invoice.
     * @see com.iguanafix.reseller.api.model.opts.DocType
     */
    public String getDocType() {
        return docType;
    }

    /**
     * Sets the value of the docType property.
     *
     * @param value Document type to be used when generating invoice.
     * @see com.iguanafix.reseller.api.model.opts.DocType
     */
    public void setDocType(String value) {
        this.docType = value;
    }

    /**
     * Gets the value of the invoiceType property.
     *
     * @return Type of invoice to be generated.
     * @see com.iguanafix.reseller.api.model.opts.InvoiceType
     */
    public String getInvoiceType() {
        return invoiceType;
    }

    /**
     * Sets the value of the invoiceType property.
     *
     * @param value Type of invoice to be generated.
     * @see com.iguanafix.reseller.api.model.opts.InvoiceType
     */
    public void setInvoiceType(String value) {
        this.invoiceType = value;
    }

    @Override
    public String toString() {
        return "{" +
                "\"billingName\":" + (billingName == null ? "null" : "\"" + billingName + "\"") + "," +
                "\"docNumber\":" + (docNumber == null ? "null" : "\"" + docNumber + "\"") + "," +
                "\"docType\":" + (docType == null ? "null" : "\"" + docType + "\"") + "," +
                "\"invoiceType\":" + (invoiceType == null ? "null" : "\"" + invoiceType + "\"") +
                "}";
    }
}
