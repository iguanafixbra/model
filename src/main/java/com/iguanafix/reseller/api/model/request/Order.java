package com.iguanafix.reseller.api.model.request;

/**
 * Payload used when calling the create order endpoint (v1/order/create).
 */
public class Order {

    private Client client;
    private ServiceData serviceData;
    private BillingData billingData;
    private String referrer;

    /**
     * Gets the value of the client property.
     * 
     * @return Data related to client.
     *     
     */
    public Client getClient() {
        return client;
    }

    /**
     * Sets the value of the client property.
     * 
     * @param value Data related to client.
     *     
     */
    public void setClient(Client value) {
        this.client = value;
    }

    /**
     * Gets the value of the serviceData property.
     * 
     * @return Data related to the service execution
     *     
     */
    public ServiceData getServiceData() {
        return serviceData;
    }

    /**
     * Sets the value of the serviceData property.
     * 
     * @param value Data related to the service execution
     *     
     */
    public void setServiceData(ServiceData value) {
        this.serviceData = value;
    }

    /**
     * Gets the value of the billingData property.
     * 
     * @return Data related to invoice generation
     *     
     */
    public BillingData getBillingData() {
        return billingData;
    }

    /**
     * Sets the value of the billingData property.
     * 
     * @param value Data related to invoice generation
     *     
     */
    public void setBillingData(BillingData value) {
        this.billingData = value;
    }

    /**
     * Gets the value of the referrer property.
     * 
     * @return An identifier that can be used by the reseller to mark where an order was generated
     *     
     */
    public String getReferrer() {
        return referrer;
    }

    /**
     * Sets the value of the referrer property.
     * 
     * @param value An identifier that can be used by the reseller to mark where an order was generated
     *     
     */
    public void setReferrer(String value) {
        this.referrer = value;
    }

    @Override
    public String toString() {
        return "{" +
                "\"client\":" + (client == null ? "null" : client) + "," +
                "\"serviceData\":" + (serviceData == null ? "null" : serviceData) + "," +
                "\"billingData\":" + (billingData == null ? "null" : billingData) + "," +
                "\"referrer\":" + (referrer == null ? "null" : "\"" + referrer + "\"") +
                "}";
    }
}
