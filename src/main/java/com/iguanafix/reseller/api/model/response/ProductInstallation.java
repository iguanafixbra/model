package com.iguanafix.reseller.api.model.response;

/**
 * Product installations available in given category.
 *
 * @see ProductInstallations
 */
public class ProductInstallation {

    private String description;
    private Integer fixedPriceInCents;
    private Long id;
    private String name;
    private String picture;
    private Integer variablePriceInCents;

    /**
     * Gets the value of the description property.
     * 
     * @return Product installation description.
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value Product installation description.
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the fixedPriceInCents property.
     *
     * @return Fixed price in cents for each installation.
     * 
     */
    public Integer getFixedPriceInCents() {
        return fixedPriceInCents;
    }

    /**
     * Sets the value of the fixedPriceInCents property.
     *
     * @param value Fixed price in cents for each installation.
     */
    public void setFixedPriceInCents(Integer value) {
        this.fixedPriceInCents = value;
    }

    /**
     * Gets the value of the id property.
     *
     * @return Product installation id.
     *
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value Product installation id.
     *
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return Product installation name.
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value Product installation name.
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the picture property.
     * 
     * @return Picture representing product installation.
     *     
     */
    public String getPicture() {
        return picture;
    }

    /**
     * Sets the value of the picture property.
     * 
     * @param value Picture representing product installation.
     *     
     */
    public void setPicture(String value) {
        this.picture = value;
    }

    /**
     * Gets the value of the variablePriceInCents property.
     *
     * @return Variable price in cents for this installation.
     *
     */
    public Integer getVariablePriceInCents() {
        return variablePriceInCents;
    }

    /**
     * Sets the value of the variablePriceInCents property.
     *
     * @param value Variable price in cents for this installation.
     *
     */
    public void setVariablePriceInCents(Integer value) {
        this.variablePriceInCents = value;
    }

    @Override
    public String toString() {
        return "{" +
                "\"description\":" + (description == null ? "null" : "\"" + description + "\"") + "," +
                "\"fixedPriceInCents\":" + (fixedPriceInCents == null ? "null" : "\"" + fixedPriceInCents + "\"") + "," +
                "\"id\":" + (id == null ? "null" : "\"" + id + "\"") + "," +
                "\"name\":" + (name == null ? "null" : "\"" + name + "\"") + "," +
                "\"picture\":" + (picture == null ? "null" : "\"" + picture + "\"") + "," +
                "\"variablePriceInCents\":" + (variablePriceInCents == null ? "null" : "\"" + variablePriceInCents + "\"") +
                "}";
    }
}
