package com.iguanafix.reseller.api.model.request;

/**
 * Service quantities hired in an order, used by ServiceData in an Order creation request.
 *
 * @see Order
 */
public class ServiceQty {

    private Integer qty;
    private Long serviceId;

    /**
     * Gets the value of the qty property.
     *
     * @return Quantity hired of given service.
     *
     */
    public Integer getQty() {
        return qty;
    }

    /**
     * Sets the value of the qty property.
     *
     * @param value Quantity hired of given service.
     *
     */
    public void setQty(Integer value) {
        this.qty = value;
    }

    /**
     * Gets the value of the serviceId property.
     *
     * @return Id of hired service.
     * 
     */
    public Long getServiceId() {
        return serviceId;
    }

    /**
     * Sets the value of the serviceId property.
     *
     * @param value Id of hired service.
     *
     */
    public void setServiceId(Long value) {
        this.serviceId = value;
    }

    @Override
    public String toString() {
        return "{" +
                "\"qty\":" + (qty == null ? "null" : "\"" + qty + "\"") + "," +
                "\"serviceId\":" + (serviceId == null ? "null" : "\"" + serviceId + "\"") +
                "}";
    }
}
