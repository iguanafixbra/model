package com.iguanafix.reseller.api.model.response;

/**
 * Installation category information.
 *
 * @see InstallationCategories
 */
public class InstallationCategory {

    private String description;
    private Long id;
    private String name;
    private String picture;

    /**
     * Gets the value of the description property.
     * 
     * @return Installation category description.
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value Installation category description.
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the id property.
     *
     * @return Installation category id.
     * 
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value Installation category id.
     * 
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return Installation category name.
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value Installation category name.
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the picture property.
     * 
     * @return Picture representing category installation.
     *     
     */
    public String getPicture() {
        return picture;
    }

    /**
     * Sets the value of the picture property.
     * 
     * @param value Picture representing category installation.
     *     
     */
    public void setPicture(String value) {
        this.picture = value;
    }

    @Override
    public String toString() {
        return "{" +
                "\"description\":" + (description == null ? "null" : "\"" + description + "\"") + "," +
                "\"id\":" + (id == null ? "null" : "\"" + id + "\"") + "," +
                "\"name\":" + (name == null ? "null" : "\"" + name + "\"") + "," +
                "\"picture\":" + (picture == null ? "null" : "\"" + picture + "\"") +
                "}";
    }
}
