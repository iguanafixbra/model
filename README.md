#IguanaFix Reseller Api Models

Java implementation of the models used by IguanaFix's Reseller Api.

To see the API documentation please click [here](https://iguanafixresellerapi.docs.apiary.io)!